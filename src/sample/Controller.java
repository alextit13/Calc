package sample;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;

public class Controller {
    public TextField textField;
    public Label Status;
    public ImageView one;
    public ImageView two;
    public ImageView three;
    public ImageView plus;
    public ImageView fore;
    private String stroke = "";
    private String str1 = "";
    private String strITOG = "";
    private int ITOG = 0;
    private int I;
    private int II;
    private char[]arr;


    public void one(MouseEvent mouseEvent) {
        stroke = textField.getText() + "1";
        textField.setText(stroke);
    }

    public void two(MouseEvent mouseEvent) {
        stroke = textField.getText() + "2";
        textField.setText(stroke);
    }

    public void three(MouseEvent mouseEvent) {
        stroke = textField.getText() + "3";
        textField.setText(stroke);
    }

    public void Plus(MouseEvent mouseEvent) {
        I = Integer.parseInt(stroke);
        str1 = stroke + "+";
        textField.clear();
        stroke = "";
        Status.setText("Введите второе число");
    }

    public void fore(MouseEvent mouseEvent) {
        stroke = textField.getText() + "4";
        textField.setText(stroke);
    }

    public void five(MouseEvent mouseEvent) {
        stroke = textField.getText() + "5";
        textField.setText(stroke);
    }

    public void six(MouseEvent mouseEvent) {
        stroke = textField.getText() + "6";
        textField.setText(stroke);
    }

    public void Minus(MouseEvent mouseEvent) {
        I = Integer.parseInt(stroke);
        str1 = stroke + "-";
        textField.clear();
        stroke = "";
        Status.setText("Введите второе число");
    }

    public void sewen(MouseEvent mouseEvent) {
        stroke = textField.getText() + "7";
        textField.setText(stroke);
    }

    public void eight(MouseEvent mouseEvent) {
        stroke = textField.getText() + "8";
        textField.setText(stroke);
    }

    public void nine(MouseEvent mouseEvent) {
        stroke = textField.getText() + "9";
        textField.setText(stroke);
    }

    public void Umnoz(MouseEvent mouseEvent) {
        I = Integer.parseInt(stroke);
        str1 = stroke + "*";
        textField.clear();
        stroke = "";
        Status.setText("Введите второе число");
    }

    public void Ce(MouseEvent mouseEvent) {
        stroke = "";
        textField.setText(stroke);
    }

    public void Zero(MouseEvent mouseEvent) {
        stroke = textField.getText() + "0";
        textField.setText(stroke);
    }

    public void Ravno(MouseEvent mouseEvent) {
        strITOG = str1 + stroke;
        II = Integer.parseInt(stroke);
        arr = strITOG.toCharArray();
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == '+'){
                ITOG = I + II;
                textField.setText(ITOG + "");
            }
            if (arr[i] == '-'){
                ITOG = I - II;
                textField.setText(ITOG + "");
            }
            if (arr[i] == '*'){
                ITOG = I * II;
                textField.setText(ITOG + "");
            }
            if (arr[i] == '/'){
                if (II == 0){
                    textField.setText("Делить на 0 нельзя!");
                    break;
                }
                ITOG = I / II;
                textField.setText(ITOG + "");
            }
        }
        Status.setText("  ^^^  Результат");
    }
    public void Delit(MouseEvent mouseEvent) {
        I = Integer.parseInt(stroke);
        str1 = stroke + "/";
        textField.clear();
        stroke = "";
        Status.setText("Введите второе число");
    }

    public void oneEntered(MouseEvent mouseEvent) {
        one.setScaleX(1.5);
        one.setScaleY(1.5);
    }

    public void oneExisted(MouseEvent mouseEvent) {
        one.setScaleX(1);
        one.setScaleY(1);
    }

    public void twoEntered(MouseEvent mouseEvent) {
        two.setScaleX(1.5);
        two.setScaleY(1.5);
    }

    public void twoExisted(MouseEvent mouseEvent) {
        two.setScaleX(1);
        two.setScaleY(1);
    }
    public void threeEntered(MouseEvent mouseEvent) {
        three.setScaleX(1.5);
        three.setScaleY(1.5);
    }
    public void threeExited(MouseEvent mouseEvent) {
        three.setScaleX(1);
        three.setScaleY(1);
    }

    public void plusEn(MouseEvent mouseEvent) {
        plus.setScaleX(1.5);
        plus.setScaleY(1.5);
    }

    public void plusEx(MouseEvent mouseEvent) {
        plus.setScaleX(1);
        plus.setScaleY(1);
    }

    public void forEn(MouseEvent mouseEvent) {
        fore.setScaleX(1.5);
        fore.setScaleY(1.5);
    }

    public void forEx(MouseEvent mouseEvent) {
        fore.setScaleX(1);
        fore.setScaleY(1);
    }
}
